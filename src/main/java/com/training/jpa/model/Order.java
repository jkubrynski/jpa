package com.training.jpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.annotations.BatchSize;

/**
 * @author Jakub Kubrynski
 */
@Entity
@Table(name = "Orders")
public class Order extends AbstractEntity {

	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "order_id")
//	@BatchSize(size = 3)
	private Collection<Product> products = new ArrayList<Product>();

	public void addProduct(Product product) {
		products.add(product);
	}

	public Collection<Product> getProducts() {
		return products;
	}
}
