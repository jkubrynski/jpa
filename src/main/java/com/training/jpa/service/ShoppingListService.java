package com.training.jpa.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

import com.training.jpa.model.Order;
import com.training.jpa.model.Product;

import org.springframework.stereotype.Service;

@Service
@Transactional
public class ShoppingListService {

	@PersistenceContext
	private EntityManager entityManager;

	public void saveProduct(Product product) {
		entityManager.persist(product);
	}

	public void saveOrder(Order order) {
		entityManager.persist(order);
	}

	public Product findProduct(int productId) {
		return entityManager.find(Product.class, productId);
	}

	public Order findOrder(int orderId) {
		return entityManager.find(Order.class, orderId);
	}

	public List<Product> findProductsInCategory(String category) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Product> query = cb.createQuery(Product.class);
		Root<Product> p = query.from(Product.class);
		query.select(p).where(cb.equal(p.get("category"), category));
		return entityManager.createQuery(query).getResultList();
	}

	public void updatePrice(int productId, int newPrice) {
		Product product = findProduct(productId);
		product.setPrice(newPrice);
	}

	public void addProductToOrder(int orderId, int productId) {
		Order order = findOrder(orderId);
		Product product = findProduct(productId);
		order.addProduct(product);
	}
}
