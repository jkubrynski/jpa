package com.training.jpa;

import com.training.jpa.model.Order;
import com.training.jpa.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jakub Kubrynski
 */
public class OrderBuilder {

	private List<Product> products = new ArrayList<>();

	static OrderBuilder anOrder() {
		return new OrderBuilder();
	}

	OrderBuilder withProduct(Product product) {
		products.add(product);
		return this;
	}

	Order build() {
		Order order = new Order();
		products.forEach(order::addProduct);
		return order;
	}
}
