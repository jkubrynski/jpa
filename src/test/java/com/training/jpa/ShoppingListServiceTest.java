package com.training.jpa;

import com.training.jpa.config.DatabaseConfig;
import com.training.jpa.model.Order;
import com.training.jpa.model.Product;
import com.training.jpa.service.ShoppingListService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DatabaseConfig.class)
public class ShoppingListServiceTest {

	@Inject
	ShoppingListService sut;

	@Test
	public void shouldSaveProductIntoDatabase() {
		// given
		Product product = new Product("beer", "drinks", 3);

		// when
		sut.saveProduct(product);

		// then
		Product productFromDB = sut.findProduct(product.getId());
		assertThat(productFromDB.getName()).isEqualTo("beer");
		assertThat(productFromDB.getCategory()).isEqualTo("drinks");
		assertThat(productFromDB.getPrice()).isEqualTo(3);

	}

	@Test
	public void shouldFindAllProductsFromCategory() {
		// given
		sut.saveProduct(new Product("bread", "breadstuff", 2));
		sut.saveProduct(new Product("bun", "breadstuff", 1));
		sut.saveProduct(new Product("ham", "meat", 10));

		// when
		List<Product> products = sut.findProductsInCategory("breadstuff");

		// then
		assertThat(products).hasSize(2);
		assertThat(products).extracting("category").containsOnly("breadstuff");
	}


	@Test
	public void shouldUpdateProductPrice() {
		//given
		Product beer = new Product("beer", "drinks", 3);
		sut.saveProduct(beer);
		Product wine = new Product("wine", "drinks", 20);
		sut.saveProduct(wine);

		// when
		sut.updatePrice(beer.getId(), 5);

		// then
		assertThat(sut.findProduct(beer.getId()).getPrice()).isEqualTo(5);
		assertThat(sut.findProduct(wine.getId()).getPrice()).isEqualTo(20);
	}

	@Transactional
	@Test
	public void shouldAddProductToOrder() {
		// given
		Product beer = new Product("beer", "drinks", 3);
		sut.saveProduct(beer);
		Order order = new Order();
		sut.saveOrder(order);

		// when
		sut.addProductToOrder(order.getId(), beer.getId());

		// then
		Order orderFromDb = sut.findOrder(order.getId());
		assertThat(orderFromDb.getProducts()).extracting("id").containsOnly(beer.getId());
	}
}
